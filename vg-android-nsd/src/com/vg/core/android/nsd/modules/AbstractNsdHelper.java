package com.vg.core.android.nsd.modules;

import java.io.IOException;

import android.content.Context;
import android.net.nsd.NsdManager;
import android.net.nsd.NsdManager.DiscoveryListener;
import android.net.nsd.NsdManager.RegistrationListener;
import android.net.nsd.NsdManager.ResolveListener;
import android.net.nsd.NsdServiceInfo;

public abstract class AbstractNsdHelper implements DiscoveryListener, RegistrationListener, ResolveListener {
	
	private final String mSuggestedName;
	private final NsdServiceInfo mServiceInfo;
	private final NsdManager mManager;
	
	public AbstractNsdHelper(
			final Context context,
			final String suggestedServiceName,
			final String serviceType,
			final int port)
					throws IOException {
		mSuggestedName = suggestedServiceName;
		mManager = (NsdManager)context.getSystemService(Context.NSD_SERVICE);
		
		mServiceInfo = new NsdServiceInfo();
		mServiceInfo.setServiceName(suggestedServiceName);
		mServiceInfo.setServiceType(serviceType);
		mServiceInfo.setPort(port);
	}
	
	public int port() {
		return mServiceInfo.getPort();
	}
	
	public String name() {
		return mServiceInfo.getServiceName();
	}
	
	public String type() {
		return mServiceInfo.getServiceType();
	}
	
	public void register() {
		mManager.registerService(mServiceInfo, NsdManager.PROTOCOL_DNS_SD, this);
		mManager.discoverServices(type(), NsdManager.PROTOCOL_DNS_SD, this);
	}
	
	public void unregister() {
		mManager.stopServiceDiscovery(this);
		mManager.unregisterService(this);
	}

	@Override
	public void onServiceFound(final NsdServiceInfo service) {
		if (!service.getServiceType().equals(type())) {
			return;
		} else if (service.getServiceName().equals(mSuggestedName)) {
			return;
		} else if (service.getServiceName().contains(mSuggestedName)) {
			mManager.resolveService(service, this);
		}
	}

	@Override
	public void onStartDiscoveryFailed(final String serviceType, final int errorCode) {
		mManager.stopServiceDiscovery(this);
	}

	@Override
	public void onStopDiscoveryFailed(final String serviceType, final int errorCode) {
		mManager.stopServiceDiscovery(this);
	}

}
