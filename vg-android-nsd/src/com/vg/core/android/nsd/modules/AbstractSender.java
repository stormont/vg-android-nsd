package com.vg.core.android.nsd.modules;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

import com.vg.core.android.nsd.interfaces.ISender;

public abstract class AbstractSender <T> implements Runnable, ISender<T> {

    private final BlockingQueue<T> mQueue;
    
    public AbstractSender(final int capacity) {
        mQueue = new ArrayBlockingQueue<T>(capacity);
    }

	@Override
	public void run() {
        while (true) {
            try {
                send(mQueue.take());
            } catch (final InterruptedException e) {
            	this.onInterrupted(e);
            }
        }
	}

}
