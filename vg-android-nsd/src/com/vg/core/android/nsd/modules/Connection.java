package com.vg.core.android.nsd.modules;

import java.io.IOException;
import java.net.Socket;

public class Connection {
	
	private final AbstractSender<?> mSender;
	private final AbstractReceiver<?> mReceiver;
	
	private Socket mSocket;
    private Thread mSendThread;
    private Thread mRcvThread;
    
    public Connection(final AbstractSender<?> sender, final AbstractReceiver<?> receiver) {
    	mSender = sender;
    	mReceiver = receiver;
    }
	
	protected Socket socket() {
		return mSocket;
	}
	
	public void tearDown() throws IOException {
        if (mSendThread != null) {
        	mSendThread.interrupt();
        	mSendThread = null;
        }
        
        if (mRcvThread != null) {
        	mRcvThread.interrupt();
        	mRcvThread = null;
        }
        
		if (mSocket != null) {
			mSocket.close();
			mSocket = null;
		}
	}

    protected synchronized void setSocket(final Socket socket) throws IOException {
        if (socket == null) {
        	throw new IllegalArgumentException("socket is null");
        }
        
        tearDown();
        
        mSocket = socket;
        
        mSendThread = new Thread(mSender);
        mSendThread.start();
        
        mRcvThread = new Thread(mReceiver);
        mRcvThread.start();
    }

}
