package com.vg.core.android.nsd.modules;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import com.vg.core.android.nsd.interfaces.IReceiver;

public abstract class AbstractReceiver <T> implements Runnable, IReceiver<BufferedReader> {
	
	private final Socket mSocket;
	
	public AbstractReceiver(final Socket socket) {
		mSocket = socket;
	}

	@Override
	public void run() {
        try {
            final BufferedReader input = new BufferedReader(new InputStreamReader(mSocket.getInputStream()));
            
            while (!Thread.currentThread().isInterrupted()) {
                this.receive(input);
            }
            
            input.close();
        } catch (final IOException e) {
        	this.onFailure(e);
        }
	}

}
