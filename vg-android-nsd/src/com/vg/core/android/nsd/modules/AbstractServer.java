package com.vg.core.android.nsd.modules;

import java.io.IOException;
import java.net.ServerSocket;

import com.vg.core.android.nsd.interfaces.IServer;


public abstract class AbstractServer implements Runnable, IServer {
	
    private ServerSocket mServerSocket;
    private Thread mThread;
    private int mPort = 0;

    public AbstractServer() {
        mThread = new Thread(this);
        mThread.start();
    }
    
    public int port() {
    	return mPort;
    }

    public void tearDown() throws IOException {
        mThread.interrupt();
        
        if (mServerSocket != null) {
        	mServerSocket.close();
        	mServerSocket = null;
        }
    }

    @Override
    public void run() {
        try {
            mServerSocket = new ServerSocket(mPort);
            mPort = mServerSocket.getLocalPort();
            
            while (!Thread.currentThread().isInterrupted()) {
                this.onSetSocket(mServerSocket.accept());
            }
        } catch (final IOException e) {
        	this.onConnectionFailed(e);
        }
    }

}
