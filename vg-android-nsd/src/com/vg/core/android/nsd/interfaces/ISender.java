package com.vg.core.android.nsd.interfaces;

public interface ISender <T> {
	
	void send(T data);
	void onInterrupted(InterruptedException e);

}
