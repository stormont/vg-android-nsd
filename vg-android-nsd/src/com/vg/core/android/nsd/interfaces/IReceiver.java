package com.vg.core.android.nsd.interfaces;

import java.io.IOException;

public interface IReceiver <T> {

	void receive(T data);
	void onFailure(IOException e);
	
}
