package com.vg.core.android.nsd.interfaces;

import java.io.IOException;
import java.net.Socket;

public interface IServer {

	void onSetSocket(Socket s);
	void onConnectionFailed(IOException e);

}
